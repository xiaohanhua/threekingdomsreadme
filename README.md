**Name TBD README - Table of Contents**  
  
  
  
[TOC]
  
  
# About
----------------------------
Placeholder


# Downloading the game
----------------------------
Windows 10 Build: [ThreeKingdomsAlpha0.1.zip](https://drive.google.com/file/d/1CL3qJ0dlMkfqTwLk9ESI705DAWYym6IZ/view?usp=sharing) 

MacOS Build: Coming soon...


# Feedback/Support
---------------------------
Discord link to be provided in future releases


# Release Notes

### Alpha 0.1
----------------------------

* Prototype release for initial testing/feedback


# Campaign Map

### Controls
----------------------------

* Arrow keys: scroll map
* Left click: select/de-select army/city
* Right click: move selected army (if applicable)
* "S" key: save the game
* "Q" key: quit to main menu
* "TAB" key: zoom camera to the next army you control
* "TAB" while holding "SHIFT": zoom camera to the next city you control
* "ENTER": end turn
* "SPACEBAR": toggle army movement animation speed (default slowest; press once for medium; press twice for fastest; press again to reset to slowest)


### Gold & Food
----------------------------
![revenue_food](https://bitbucket.org/xiaohanhua/threekingdomsreadme/raw/d46b53d788cbdb236c215cbbfe10fcce85473434/Screenshots/gold_food_icons.png)

##### Gold
----------------------------
The amount of gold in your reserves is indicated by the number next to the gold icon in the top left corner of the screen. 
The number before the parenthesis is how much gold you currently have, and the number in the parenthesis is your projected
income for next turn. Hover over this icon to see a breakdown of your revenue, upkeep cost, and net income.

There are 2 primary sources for income: taxes and buildings. Buildings such as Markets can be upgraded to produce higher
per-turn revenue. The amount of taxes collected depends on the combined population of all your cities, which grows
over time based on the people's happiness and your food surplus.

##### Food
----------------------------
The amount of food in your storage is indicated by the number next to the bag of rice icon to the right of the gold icon.
The number before the parenthesis is how much food you currently have, and the number in the parenthesis is your projected
food surplus for next turn. Hover over this icon to see a breakdown of your food production, consumption, and net surplus.

You can increase your food production by upgrading farms located in your cities. Food consumption is based on how many
units you have recruited overall.

If your food storage drops below 0, indicating a nationwide famine, people will quickly become unhappy and flee from your
cities, causing your tax revenue to decrease sharply.



### Selecting an army
----------------------------
Left click on a flag representing an army led by one of your generals to select it. After selecting an army, you will
be shown information about the army on the right, including the commander leading the army, commander's stats/skills, 
units within the army, as well as an option to manage the army.

As you move your cursor around the map, you will also be shown the shortest reachable path (in green) to the grid where 
your cursor is located. Right click to move the selected army to that location. The maximum distance an army can travel
is determined by the action points of its commander.

You can also click on enemy armies to view information about them, but you will not be able to control them.

![army_selection](https://bitbucket.org/xiaohanhua/threekingdomsreadme/raw/82e2a3c50f7f3b4aa0e2a33f0aafb29c522f6c1d/Screenshots/army_selection.png)


### Terrain
----------------------------
There are 4 types of terrain you will encounter on the campaign map:

* Normal: these are normal, walkable tiles that cost 1 action point per grid.
* Small Obstacles: these include forests and smaller rocky regions that cost 3 action points per grid.
* Water: costs 4 action points per grid.
* Barriers: these tiles, like mountainous regions, are not traversable.

![terrain](https://bitbucket.org/xiaohanhua/threekingdomsreadme/raw/ec87b18e5271f1383f93159a952cee0918486a02/Screenshots/terrain.png)



### Embedding an army
----------------------------
If you try to move one of your armies near a city that is currently unoccupied, you will see a Shield icon, indicating that
you will embed your army into the city. Any garrisoned troops you have inside the city will automatically be merged into
this army. Furthermore, it means that when an enemy army attacks your city, you will be able to defend it with your commander
rather than a garrison without a general.

![embedding_in_city](https://bitbucket.org/xiaohanhua/threekingdomsreadme/raw/d70d4fcbe4fac816b2275f7a1a7f329f139c7d14/Screenshots/army_embed_in_city.png)


### Attacking an enemy army/city
----------------------------
Select one of your armies and hover over an enemy army or city; you will see a Cross-Swords icon, indicating that
you will engage in battle after moving to that location.

![attack_enemy_army](https://bitbucket.org/xiaohanhua/threekingdomsreadme/raw/d70d4fcbe4fac816b2275f7a1a7f329f139c7d14/Screenshots/army_attack_enemy.png)

![attack_enemy_city](https://bitbucket.org/xiaohanhua/threekingdomsreadme/raw/d70d4fcbe4fac816b2275f7a1a7f329f139c7d14/Screenshots/army_attack_enemy_city.png)


Before the battle begins, you will be shown a preview screen with details of the troops on both sides. At this point,
if you have enough action points remaining this turn, you can choose to "Retreat" (costs 8 action points).If an enemy 
attacks you during their turn and you choose to retreat, you will spend 8 action points each time, which will reduce 
the distance you can travel next turn.

![battle_preview](https://bitbucket.org/xiaohanhua/threekingdomsreadme/raw/d70d4fcbe4fac816b2275f7a1a7f329f139c7d14/Screenshots/battle_preview_with_general.png)

Jump down to the Battle section of this guide to learn more about how battles work.


##### Capturing an enemy general
----------------------------
Depending on the outcome of the battle, you may capture the enemy general. You will always have the option to Kill or Release them.

* Killing the general removes them from the game permanently. 
* Releasing them will decrease their loyalty to their current lord and increases the likelihood that you will be able to recruit them the next time you capture them. When you release a general, they automatically return to the nearest city that belongs to their country.
* If their loyalty is low enough, you will be given the option to recruit them, after which you will be able to control them.

![capture_enemy_general](https://bitbucket.org/xiaohanhua/threekingdomsreadme/raw/ec87b18e5271f1383f93159a952cee0918486a02/Screenshots/post_battle_enemy_general_captured.png)


### Manage army
----------------------------
If you select an army and choose "Manage", you will be taken to the army management screen, where you can see more detailed
information about your commander and spend any upgrade points that you obtain from leveling up to boost your general's stats.
Your general gains experience by winning battles, though they can also gain a little bit of experience from losing.

You also have the option to disband certain units (cannot be undone!) if you want to reduce your army upkeep cost.

![army_management](https://bitbucket.org/xiaohanhua/threekingdomsreadme/raw/d70d4fcbe4fac816b2275f7a1a7f329f139c7d14/Screenshots/army_management.png)


##### General Stats
----------------------------
* Loyalty: determines probability that your commander defects to an enemy after a battle. Each time your general loses a battle, his/her loyalty will decrease by 5.
* Combat Prowess: determines the upper limit of your army's morale (if morale reaches 0, you lose the battle). Also influences the chances of capturing an enemy general after a victory/avoiding capture when defeated. Costs 1 upgrade point to increase by 1.
* Intellect: determines how quickly your command points regenerate. Costs 1 upgrade point to increase by 1.
* Command: determines the upper limit of your command, which is used to deploy units onto the battlefield. Costs 1 upgrade point to increase by 1.
* Action Pts: determines how far your army can travel on the campaign map each turn. Costs 3 upgrade points to increase by 1.

![upgrade_general_stats](https://bitbucket.org/xiaohanhua/threekingdomsreadme/raw/ff6470718401d7fb6b2a9bafc6c00a9c0c0b84c5/Screenshots/commander_level_up.png)

##### General Special Attacks
----------------------------
Special attacks can be activated during battle and have a cool-down period. Hover over the icons for more details.
It is advised that you modify your army composition to maximize the benefits from the general's special attacks.
(i.e. place more archers in an army led by someone with 'Expert Marksmanship')

##### General Skills
----------------------------
Skills are passive and need not be activated. Their effects are automatically applied when appropriate.


### Army upkeep
----------------------------
Each unit's upkeep cost is equal to 1% of its recruiting cost (i.e. Sword Militia costs 20 gold to recruit, so for each
Sword Militia you have, your upkeep cost is .2 gold per turn). The upkeep cost for each commander is 50 times his/her current level.



### Manage city
----------------------------
If you select a city, you will be shown some basic stats such as population and happiness and the garrison (if any).
If you choose "Manage", you will be taken to the city management screen, where you will have the option
to view/upgrade existing buildings and recruit units.

![manage_city](https://bitbucket.org/xiaohanhua/threekingdomsreadme/raw/ec87b18e5271f1383f93159a952cee0918486a02/Screenshots/city_selection.png)


##### Buildings
----------------------------
There are 4 basic buildings you can upgrade.

* Walls: having stronger walls makes it more difficult for the enemy to capture the city. Upgrading walls not only increases its "health" but also provides additional retainer units that are added to your existing garrisoned troops free of cost!
* Barracks: determines the maximum number of units you can recruit from the city per turn.
* Farms: determines food production from the city.
* Market: determines income generated per turn and happiness of the citizens in this city.

Hover over each to view information. Click on a building to select it, and click on "Upgrade" to upgrade.
If you don't have enough gold or if the building cannot be upgraded further, the Upgrade button will be disabled.

![buildings](https://bitbucket.org/xiaohanhua/threekingdomsreadme/raw/ec87b18e5271f1383f93159a952cee0918486a02/Screenshots/city_management_building_tooltips.png)
![buildings_upgrade](https://bitbucket.org/xiaohanhua/threekingdomsreadme/raw/ec87b18e5271f1383f93159a952cee0918486a02/Screenshots/city_management_building_upgrade.png)



##### Recruitment
----------------------------
Click the Barracks and then "Recruit" to open the recruitment interace. Here, you will see a list of units that you can recruit
from this city along with stats of each unit. Click on the unit icons to select different units. 
Use the "Recruit" button to recruit. You can toggle the quantity (increments of 10) that you want to recruit.

![recruitment](https://bitbucket.org/xiaohanhua/threekingdomsreadme/raw/ec87b18e5271f1383f93159a952cee0918486a02/Screenshots/city_management_recruit.png)

Units you recruit will show up in the city's garrison. Keep in mind that while units can remain inside a city to defend it even
without the presence of a general, the garrisoned troops cannot leave the city without a commander leading it.






# Battles

### Controls
----------------------------

* Left/Right arrow keys: toggle selected unit
* Up/Down arrow keys: toggle deployment slot
* Spacebar: deploy selected unit
* Left mouse click: click on special attack icons to activate the special attacks
* Hover over: move cursor over unit icons/special attack icons to view detailed information
* "P" key: pause/resume
* Numbers 1-8 : shortcut to switch selected unit (top row is 1-4, bottom row is 5-8)
* CONTROL + 1 or 2: shortcut to active general's special attack 1 or 2, if applicable
* Moving cursor to left/right end of the screen will shift the camera horizontally (to see extended areas of the battlefield)


### Winning Condition
----------------------------
To win the battle, either:

* Reduce the opponent's morale to 0

OR

* Opponent has no more undeployed units and no more units on the battlefield (unless they are defending against a siege attack)


### Interface
----------------------------
Morale is indicated by the red bar, and command is represented by the yellow bar. Each time you deploy a unit onto the battlefield,
you will use up a certain number of command points, depending on the unit selected. The yellow command bar will shorten and replenish
towards the maximum over time. This replenish rate is determined by your commander's intellect/command/skills but also the
total number of troops engaged in the battle. If the size of your army is larger than the opponent's, you will receive a boost
in your command point replenish rate.

There are 8 possible deployment slots to choose from. The current deployment slot is indicated by the flashing yellow arrow. 
Use up/down arrow keys to switch deployment slots.

![battle_overview](https://bitbucket.org/xiaohanhua/threekingdomsreadme/raw/d88220738703cea24cd1002a2ccfb8b7bc37351c/Screenshots/battle_overview.png)


### Morale
----------------------------
There are 2 standard ways to lower the opponent's morale.

* One of your units walks into the opponent's deployment zone. Your opponent's morale will decrease by an amount proportional to the remaining health of the unit.
* Consecutive kill streaks; you will lower the opponent's morale (and receive a boost of the same magnitude) by:

2 for a 3-kill streak; 3 for a 5-kill streak; 5 for a 10-kill streak


### Consecutive Deployment Limit
----------------------------
There is a limit on the number of units you can deploy within the same deployment slot in a short period of time. By default,
it is 4 units every 2 seconds for the same slot. This is to prevent slot spamming since units (especially melee units) deployed 
within close proximity of each other have an advantage over units that are deployed alone when they engage in combat.


### Siege Defense
----------------------------
When an army attacks a city, the garrisoned troops simply need to defend the walls. When an enemy units gets close enough to the
walls, they will begin to attack the wall, causing damage over time. In siege defenses, the defending side's morale is determined
by the strength of the walls of the city and thus has an advantage. Therefore, it is recommended to bring troops that are 
stronger, either in number or quality, than the garrisoned troops within a city if you plan to capture a city.


### Post Battle Calculations
----------------------------
After battle, all troops deployed onto the battlefield will be lost. If the losing side has remaining troops, its army size
will be reduced by a certain percentage depending on __the morale of the winning side at the end of battle__ and __the size of
the winning army__. If, following these calculations, the losing side still has leftover units, the army will be forced to
retreat on the campaign map.

If you completely wipe out the enemy army, there is a good chance you will capture the enemy commander.




# Attribution
---------------------------
[LPC Sprites - detailed attribution](https://bitbucket.org/xiaohanhua/threekingdomsreadme/src/master/LPC_sprites_all_credits.csv)



